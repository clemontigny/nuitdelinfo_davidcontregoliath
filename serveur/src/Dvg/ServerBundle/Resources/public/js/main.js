$(function(){
    
        var json = {
            "famille" : "linge",
            "concepts" : [
                {
                    "name" : "chapeau",
                    "url" : "http://david-contre-goliath.c-mnt.com/icone/Chapeau.png"
                },
                {
                    "name" : "femme",
                    "url" : "http://david-contre-goliath.c-mnt.com/icone/Femme.png"
                },
                {
                    "name" : "homme",
                    "url" : "http://david-contre-goliath.c-mnt.com/icone/Homme.png"
                },
                {
                    "name" : "costume",
                    "url" : "http://david-contre-goliath.c-mnt.com/icone/Costume.png"
                },
                {
                    "name" : "robe",
                    "url" : "http://david-contre-goliath.c-mnt.com/icone/Robe.png"
                },
                {
                    "name" : "t-shirt",
                    "url" : "http://david-contre-goliath.c-mnt.com/icone/T-shirt.png"
                },
                {
                    "name" : "chaussettes",
                    "url" : "http://david-contre-goliath.c-mnt.com/icone/Chaussettes.png"
                }
            ]
        }
        
        for (var i=0;i<json.concepts.length;i++)
        {
            $("#icones").append("<div class=\"icone\"><img src=\"" + json.concepts[i].url + "\" alt=\"" + json.concepts[i].name + "\"></div>");
        }
		
		$(".icone").draggable({
			
		});
		$(".icone").droppable();       
        
});