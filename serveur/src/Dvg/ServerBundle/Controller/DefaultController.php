<?php

namespace Dvg\ServerBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Dvg\ServerBundle\AmazonClass\AmazonProductAPI;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DvgServerBundle:Default:index.html.twig', array('name' => $name));
    }
    
    public function getAmazonProductAction($keywords)
    {
        $amazonApi = $this->container->get('dvg_server.amazon_api');
    
        try
        {
            $result = $amazonApi->searchProducts("$keywords",
                                           "",
                                           "TITLE");
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
        
        $products = array();
        foreach($result->Items->Item as $product)
        {
            $prodAttr = array(
                "title" => $product->ItemAttributes->Title,
                "price" => $product->OfferSummary->LowestNewPrice->FormattedPrice,
                "link" => $product->DetailPageURL,
                "image" => $product->MediumImage->URL
            );
            array_push($products, $prodAttr);
        }
      //  var_dump($result);
      
        $response = new JsonResponse($products);
        return $response;
       // return $this->render('DvgServerBundle:Default:index.html.twig', array('name' => $result->Items->Item->ItemAttributes->Title));
        
    }
    
}
